public class aula46 {
    public static void main(String[] args) {
        int[] array = {78, 20, 56, 89, 1}; // elementos do array
        System.out.printf("%s%8s\n","Indice","Valores"); // imprimi

        for(int i = 0; i < array.length; i++) { // looping for, variavel inteira i valor 0, i menor de que array.length, incrementar unidade
            System.out.printf("%5d%8d\n", i, array[i]); // imprimi o valor de cada posição do array , \n quebra linha


        }

    }
}
